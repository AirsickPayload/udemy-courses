package main

import (
	"time"
	"net/http"
  "fmt"
  "os"
)

func main() {
  links := []string{
    "https://google.com",
    "https://amazon.com",
    "https://stackoverflow.com",
    "https://facebook.com",
  }


  c := make(chan string)

  for _, link := range links {
    go checkLink(link, c)
  }

  for l := range c {
    go func(l string) {
      time.Sleep(5 * time.Second)
      checkLink(l,c)
    }(l)
  }
}
func checkLink(link string, c chan string) {
  _, err := http.Get(link)
  if err != nil {
    fmt.Println(link, "might be down!")
    c <- link
    os.Exit(1)
  }
  fmt.Println(link, "is up!")
   c <- link
}